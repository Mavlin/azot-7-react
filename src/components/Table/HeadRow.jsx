import React from 'react'

class HeadRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: this.props.name
        }
    }

    render() {
        const {name} = this.state;
        // console.log('render chaild')
        return (
            <span className="">
                {name}
            </span>
        )
    }
}

export default HeadRow
