import React from 'react'

class Cell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            tst: 555,
        }
        // Эта привязка обязательна для работы `this` в колбэке.
        // this.handleClick = this.handleClick.bind(this);
    }

    handleClick=(e)=>{
        // console.log(this.tst)
        console.log(this.state.tst);
        console.log(e.target)
        // this.setState(state => ({
        //     isToggleOn: !state.isToggleOn
        // }));
    };

    render() {
        const {data} = this.state;

        return (
            <div onClick={this.handleClick} className="w-100 h-100 d-flex align-items-center justify-content-center">
                <div className="row justify-content-middle align-items-left position-relative
                 m-0 w-100 h-100 align-content-center">
                    {data.component}<br/>
                    {data['id_comp']}
                </div>
            </div>
        )
    }
}

export default Cell
