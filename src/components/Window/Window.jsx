/* eslint-disable */
import React from 'react'
import axios from 'axios'

import {
    HeadRow,
    HeadColumn,
    Cell
} from "components";

class Window extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            allData: [],
            headColumns: [],
            headRows: [],
            Cells: [],
            cellWidth: 100,
            cellHeight: 100,
            cntColumns: 0,
            cntRows: 0,
            tblWidth: 0,
        }
    }

    headColumns(a) {
        let set = new Set(), size;
        a.some((item) => {
            set.add(item['location'])
        });
        size = set.size;
        // https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Set#Взаимоотношения_с_объектом_Array
        this.setState(() => {
            return {
                headColumns: [...set],
                tblWidth: size * this.state.cellWidth,
                cntColumns: size,
            }
        })
    };

    headRows(a) {
        let set = new Set(), size;
        a.some((item) => {
            set.add(item['component'])
        });
        size = set.size;
        this.setState(() => {
            return {
                headRows: [...set],
                cntRows: size,
            }
        })
    };

    Cell() {
        let set = [];
        this.state.allData.some((item) => {
            set.push(item)
        });
        console.log(set);
        // set.map((val, i)=>{
        //     console.log(val, i)
        // })
        this.setState(() => {
            return {
                Cells: set,
            }
        })
    };

    componentDidMount() {
        axios({
            method: 'get',
            url: 'init.php',
            params: {
                select: 'WorkLog',
                mode: 'getAllDataInPeriod',
                data: {
                    date: '2019-05-10',
                    duration: ''
                }
            },
            responseType: 'json',
        })
            .then(
                response => {
                    this.setState(() => {
                        return {
                            allData: response.data.data,
                        }
                    });
                    this.headColumns(response.data.data);
                    this.headRows(response.data.data);
                    this.Cell()
                }
            )
            .catch(function (error) {
                console.log(error);
            })
    }

    render() {
        const {headColumns, headRows, Cells} = this.state;
        let emptyCell = {
                width: this.state.cellWidth,
                height: this.state.cellHeight,
            },
            tblStyle = {
                width: this.state.tblWidth
            };
        return (
            <div className="overflow-auto d-flex mtbl justify-content-start justify-content-xl-center">
                <div className="row flex-nowrap m-0">
                    <div className="col-auto p-0 position-static">
                        <div className="empty-cell" style={emptyCell}>-</div>
                        {headRows.map((val, i) => {
                                return (
                                    <div style={emptyCell} key={i.toString() + val.toString()}
                                         className="columns f-size-2 d-flex align-items-center justify-content-center fw-4">
                                        <HeadColumn
                                            name={val}/>
                                    </div>
                                )
                            }
                        )
                        }
                    </div>
                    <div className="col-auto p-0 position-static">
                        <div style={tblStyle} className="row m-0">
                            <div className="col p-0 d-flex flex-wrap position-static">
                                {headColumns.map((val, i) => {
                                        return (
                                            <div style={emptyCell} key={i.toString() + val.toString()}
                                                 className="rows outline-crimson d-flex align-items-center justify-content-center fw-4">
                                                <HeadRow name={val}/>
                                            </div>
                                        )
                                    }
                                )
                                }
                                {Cells.map((val, i) => {
                                    return (
                                        <div style={emptyCell} key={i.toString()}
                                             className="my-cell outline-green position-static">
                                            <Cell data={val}/>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Window
