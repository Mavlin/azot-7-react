const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    app.use(proxy('/init*', { target: 'http://0808.ddns.net/' }));
};
